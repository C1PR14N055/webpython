<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="js/jquery-3.1.1.min.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/codemirror.css">
	<link rel="stylesheet" href="css/monokai.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<script src="js/codemirror.js"></script>
	<script src="js/python.js"></script>
	
	<script>
	var editor;

	$(function(){

		// generate editor
		editor = CodeMirror.fromTextArea(document.getElementById("code"), {
				mode: {name: "python",
				version: 2,
				singleLineStringErrors: false 
			},
			lineNumbers: true,
			indentUnit: 4,
			indentWithTabs: true,
			matchBrackets: true, 
			theme: "monokai"
		});

		// submit code ajax
		$("#exec").click(function () {
			$("#exec").prop("disabled", true); // disable btn
			$.ajax({
				type: "post",
				url: "exec_code.php",
				data: {code: editor.getValue()},
				success: function (data) {
					$("#exec").prop("disabled", false); // enable btn

					data = JSON.parse(data);
					$("#output pre").text(data["out"]);

					if (data["status"] != 0) { // if error 
						$("#output pre").css("color", "red");
					} else {
						$("#output pre").css("color", "#333");
					}
				}
			});
		});

		$(window).resize(function(){
			setEditorSize(); // set site on resize
		});

		// on change save to localStorage
		editor.on('change', editor => {
			window.localStorage.setItem("code", editor.getValue());
		});

		// on doc ready if != undefined set code from localStorage
		if (window.localStorage.getItem("code") != undefined && window.localStorage.getItem("code") != "") {
			editor.setValue(window.localStorage.getItem("code"));
		}

	});


	// editor size same as image
	function setEditorSize() {
		$(".CodeMirror").css("height", $(".mac img").css("height"));
	}

	// get code from example file
	function getCodeFromExamples(file) {
		$.ajax({
			type: "get",
			url: "read_code.php?file=" + file,
			success: function (data) {
				data = JSON.parse(data);
				editor.setValue(data["out"]);
			}
		});
	}

	$(window).on("load", function(){
		setEditorSize(); // set size once, when image is loaded
	});
</script>
<link rel="icon" type="image/png" href="img/logo.png">
<title>&raquo; H4ND50M3 C0D3 &laquo;</title>

<style>
	.container-full {
		margin: 0 auto;
		width: 100%;
	}
	#logo {
		animation: logo-anim 2s infinite;
		-webkit-animation: logo-anim 2s infinite;
		display: inline-block;
		max-width: 70px;
		margin-right: 20px;
	}
	@keyframes logo-anim {
		0% {transform: scale(1); -webkit-transform: scale(1);}
		10% {transform: scale(1); -webkit-transform: scale(1);}
		20% {transform: scale(1); -webkit-transform: scale(1);}
		30% {transform: scale(1); -webkit-transform: scale(1);}
		40% {transform: scale(1); -webkit-transform: scale(1);}
		50% {transform: scale(1.1); -webkit-transform: scale(1.1);}
		60% {transform: scale(1); -webkit-transform: scale(1);}
		70% {transform: scale(1.1); -webkit-transform: scale(1.1);}
		80% {transform: scale(1); -webkit-transform: scale(1);}
		90% {transform: scale(1); -webkit-transform: scale(1);}
		100% {transform: scale(1); -webkit-transform: scale(1);}
	}
	@-webkit-keyframes logo-anim {
		0% {transform: scale(1); -webkit-transform: scale(1);}
		10% {transform: scale(1); -webkit-transform: scale(1);}
		20% {transform: scale(1); -webkit-transform: scale(1);}
		30% {transform: scale(1); -webkit-transform: scale(1);}
		40% {transform: scale(1); -webkit-transform: scale(1);}
		50% {transform: scale(1.1); -webkit-transform: scale(1.1);}
		60% {transform: scale(1); -webkit-transform: scale(1);}
		70% {transform: scale(1.1); -webkit-transform: scale(1.1);}
		80% {transform: scale(1); -webkit-transform: scale(1);}
		90% {transform: scale(1); -webkit-transform: scale(1);}
		100% {transform: scale(1); -webkit-transform: scale(1);}
	}
	h1 {
		font-family: "monospace", "Monaco", "Menlo", "Courier", "Courier New";
		color: #eb242c;
		font-size: 4em;
		text-align: center;
		margin-top: 15px;
		margin-bottom: 0;
	}
	.CodeMirror {
		height: 750px;
	}
	.mac {
		position: relative;
	}
	.mac img {
		width: 100%;
	}
	.mac #output {
		position: absolute;
		top: 10%;
		left: 10%;
		z-index: 2;
		height: 55%;
		overflow: auto;
		width: 80%;
	}
	.space {
		margin-top: 20px;
	}
	.tac {
		text-align: center;
	}
	.btn-big {
		width: 300px;
		height: 80px;
		font-size: 2.5em;
	}
	pre {
		display: block;
		padding: 0;
		margin: 0 0 10px;
		font-size: 13px;
		line-height: 1.42857143;
		color: #333;
		word-break: break-all;
		word-wrap: break-word;
		background-color: transparent;
		border: none;
		border-radius: 0;
	}
	.fa-2 {
		font-size: 2em;
	}
	.dropdown {
		position: fixed;
		top: 10px;
		right: 10px;
		z-index: 10;
	}
</style>

</head>
<body>

	<div class="container container-full">
		<div class="dropdown">
			<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-bars fa-2" aria-hidden="true"></i></button>
			<ul class="dropdown-menu dropdown-menu-right">
				<li><a onclick="getCodeFromExamples('vars.py')" href="#!">1. Variabile</a></li>
				<li><a onclick="getCodeFromExamples('math.py')" href="#!">2. Operatii</a></li>
				<li><a onclick="getCodeFromExamples('cond.py')" href="#!">3. Conditii</a></li>
				<li><a onclick="getCodeFromExamples('loops.py')" href="#!">4. Loops</a></li>
				<li><a onclick="getCodeFromExamples('func.py')" href="#!">6. Functii</a></li>
				<li><a onclick="getCodeFromExamples('weather.py')" href="#!">7. Exemplu</a></li>
				<li><a onclick="getCodeFromExamples('diy.py')" href="#!">8. DIY</a></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h1><img id="logo" alt="logo" src="img/logo.png">H4ND50M3 C0D3</h1>
			</div>
			<div class="col-sm-6 space">
				<textarea id="code" name="code">print "Hello Handsome"</textarea>
			</div>
			<div class="mac col-sm-6 space">
				<img src="img/mac.png" alt="mac">
				<div id="output"><pre>Waiting...</pre></div>
			</div>
			<div class="col-sm-12 space tac"><button id="exec" class="btn btn-big btn-success" type="submit">Executa &raquo;</button></div>
		</div>
	</div>

</body>
</html>