'''
Loops, cand vrem sa facem ceva de mai multe si sa scriem doar o data

'''

# loop-ul for
# pentru fiecare i in [1, 2, 3, 4, 5]
for i in range(5):
	print "Hello"

# while (cat timp, pana se indeplineste o conditie)
numar = 1
while numar < 10:
	print numar
	numar = numar + 1