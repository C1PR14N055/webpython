'''
Operatii matematice 
 
'''

# Adunare
print "1 + 1 =", 1 + 1

#Scadere
print "7 - 2 =", 7 - 2

#Inmultire
print "2 * 4 =", 2 * 4

#Impartire
print "6 / 2 =", 6 / 2

#Modulo (restul impratirii)
print "5 % 2 =", 5 % 2

#Ridicare la putere
print "2 ** 8 =", 2 ** 8