'''
Variabile si tipuri de variabile

'''

x = 1234 # numar intreg (pozitiv sau negativ)
y = 3.141592 # numar cu zecimale (pozitiv sau negativ)
name = "Rex" # string (sir de caractere / text)
isAnimal = True # boolean
hasTail = False # boolean
colors = ["rosu", "gri", "mov"] # lista
dex = {"nrPicioare": 4, "specie": "caine"} # dictionar

print x
print y
print name
print isAnimal
print hasTail
print colors
print dex