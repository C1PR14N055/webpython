'''
Conditii if / else

'''

#definitie variabile
zoranIsHandsome = True
bogdanHatesPizza = False

#conditie doar cu if
if zoranIsHandsome == True:
	print "Yupp, Zoran is handsome"

#conditie cu if si else
if bogdanHatesPizza == True:
	print "Bogdan hates pizaa :("
else:
	print "Bogdan loves pizza :)"