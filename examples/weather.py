# -*- coding: utf-8 -*-
import urllib2, urllib, json, base64

ip_location_url = "http://ip-api.com/json"
print "-" * 50
print "Se detecteaza locatia dupa IP..."
resp = urllib.urlopen(ip_location_url)
try:
	location = json.loads(resp.read())
except:
	location["city"] = base64.b64decode("QXJhZA==")
print "Locatie:", location["city"]
print "-" * 50
q_url = "https://query.yahooapis.com/v1/public/yql?"
q = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" \
	+ location["city"] + ", " + location["countryCode"] + "')"

q_url = q_url + urllib.urlencode({'q':q}) + "&format=json"
result = urllib2.urlopen(q_url).read()
data_weather = json.loads(result)
data_weather = data_weather['query']['results']['channel']

print "Data:\t\t", data_weather['item']['condition']['date']
print "Rasarit:\t", data_weather['astronomy']['sunrise']
print "Apus:\t\t", data_weather['astronomy']['sunset']
print "Temp:\t\t", int((int(data_weather['item']['condition']['temp']) - 32) / 1.8), "°C"
print "Directie Vant:\t", data_weather['wind']['direction'], "°"
print "Viteza Vant:\t", data_weather['wind']['speed'], "km/h"
print "Starea:\t\t", data_weather['item']['condition']['text']
print "-" * 50