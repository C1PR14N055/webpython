# README #

### What's this? ###
A python editor that runs in the browser **for demonstration purposes only**!

### What it isn't: ###
A secure way to do that ^

### How does it looks like? ###
![Screenshot.png](https://bitbucket.org/repo/6xEKab/images/3492090563-Screenshot.png)

### Features: ###
- Code highlighting
- Local storage to save users code in browser (prevent loss if user refreshes page)
- AJAX 
- Responsiveness
- Basic python examples
- Exit code status

### How do I get set it up? ###
```
#!bash

git clone git@bitbucket.org:C1PR14N055/webpython.git
sudp cp -r webpython/* /var/www/html/
sudo mkdir scripts/
sudo chmod 777 scripts
```
- ???
- Profit