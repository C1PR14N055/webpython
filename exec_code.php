<?php

function generateRandomString($length = 4) {
	$characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$charactersLength = strlen($characters);
	$randomString = "";
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

if (isset($_POST["code"]) && !empty($_POST["code"])) {
	$filename = "scripts/" . generateRandomString() . ".py";
	$file = fopen($filename, "w") or die('{"out": "Unable to open file: ' . $filename . '", "status": "1"}');
	$txt = $_POST["code"];
	fwrite($file, $txt);
	fclose($file);
	$resp = array();
	$resp["out"] = trim(shell_exec("python " . $filename . " 2>&1; echo $?"));
	$resp["status"] = substr($resp["out"], -1, 1);
	$resp["out"] = trim(substr($resp["out"], 0, -1));
	echo json_encode($resp);
} else {
	die('{"out": "Empty post!", "status": "1"}');
}

?>
