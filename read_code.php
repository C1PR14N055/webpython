<?php 
	if (isset($_GET["file"]) && !empty($_GET["file"])) {
		$examples_dir = "examples/";
		$file_path = $examples_dir . $_GET["file"];
		$file = fopen($file_path, "r") or die('{"out": "Unable to open file: ' . $file_path . '", "status": "1"}');
		$resp = array();
		$resp["status"] = 0;
		$resp["out"] = fread($file, filesize($file_path));
		echo json_encode($resp);
		fclose($file);
	}
?>